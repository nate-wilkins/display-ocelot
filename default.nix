########################################################################################################################
#⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀#
#⠀Author:⠀Nate-Wilkins⠀<nate-wilkins@code-null.com>⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀#
#⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀#
########################################################################################################################
{
  pkgs,
  manifest,
  gitignore,
  lib,
  dependencies,
  ...
}: (
  let
    getSrc                                        = src:
      let
        isGitIncluded = gitignore { basePath = src; extraRules = ''
          *
          !shell/
          !src/
          !.envrc.sh
          !default.nix
          !flake.nix
          !LICENSE
        ''; };
      in
        lib.cleanSourceWith {
          inherit src;
          name = "source";
          filter = isGitIncluded;
        };

    name                                                   = manifest.name;
    version                                                = manifest.version;
    description                                            = manifest.description;
    author                                                 = manifest.author;

    script                                                 = pkgs.writeShellScriptBin manifest.name (builtins.readFile ./src/index.sh);
  in (
    pkgs.stdenv.mkDerivation(
      {
        pname                                              = name;
        version                                            = version;

        src                                                = getSrc ./.;

        nativeBuildInputs                                  = with pkgs; [
          makeWrapper
        ] ++ dependencies;

        buildPhase                                         = ''
           mkdir -p "$out/bin"
           cp "${script}/bin/${manifest.name}" "$out/bin/"
           chmod a+x "$out/bin/${manifest.name}"
        '';

        postInstall                                        = ''
          substituteInPlace "$out/bin/${name}" \
            --replace '_DO_NAME_'        'DO_NAME="${name}"' \
            --replace '_DO_VERSION_'     'DO_VERSION="${version}"' \
            --replace '_DO_DESCRIPTION_' 'DO_DESCRIPTION="${description}"' \
            --replace '_DO_AUTHOR_'      'DO_AUTHOR="${author}"'

          wrapProgram "$out/bin/${name}" \
            --set PATH ${lib.makeBinPath (
              dependencies ++ [ "$out" ]
            )}
        '';
      }
    )
  )
)

