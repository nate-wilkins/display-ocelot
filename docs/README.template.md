# <%{ name }%>

> <%{ description }%>

## Usage

```bash
<%{ asciinema }%>
```

[![Demo](./examples/demo.gif)](https://asciinema.org/a/647593)

### Show

> Display the ocelot!

```
nix run gitlab:nate-wilkins/display-ocelot
```

## Roadmap

- Theming

## Development

Workflows use `.envrc.sh` which are defined in `shell/default.nix`.

```
git clone git@gitlab.com:nate-wilkins/display-ocelot.git
cd display-ocelot
source .envrc.sh
help
```

## Contributions

| Author  | Estimated Hours |
| ------------- | ------------- |
<%#authors%>| [![<%name%>](https://github.com/<%name%>.png?size=64)](https://github.com/<%name%>) | <p align="right"><%hours%> Hours</p> |
<%/authors%>

